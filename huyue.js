/**
 * js 常用库
 */
(function(window){
	var huyue = huyue || {};

    // 重写js 乘法运算
    huyue.accMul = function(arg1,arg2){
        var m=0,s1=arg1.toString(),s2=arg2.toString();
        try{m+=s1.split(".")[1].length}catch(e){}
        try{m+=s2.split(".")[1].length}catch(e){}
        return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
    } 

    // 开启窗口上下滑动
    huyue.bodyScroll = function() {
        $("html").css({"height": "auto", "overflow": "visible"});
        document.body.style.height = "auto";
        document.body.style.overflow = "visible";
    }

    // 禁止窗口上下滑动
    huyue.bodyNoScroll = function() {
        $("html").css({"height": "100%", "overflow": "hidden"});
        document.body.style.height = "100%";
        document.body.style.overflow = "hidden";
    }

	// 验证中文名称
    huyue.isChinaName = function(name) {
        var pattern = /^[\u4E00-\u9FA5_a-zA-Z]{2,15}$/;
        return pattern.test(name);
    }

    // 验证手机号
    huyue.isPhoneNo = function(phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    }

    // 验证身份证
    huyue.isCardNo = function(code){
        var city={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
        var tip = "";
        var pass= true;

        if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
            tip = "身份证号格式错误";
            pass = false;
        }

        else if(!city[code.substr(0,2)]){
            tip = "地址编码错误";
            pass = false;
        }
        else{
            //18位身份证需要验证最后一位校验位
            if(code.length == 18){
                code = code.split('');
                //∑(ai×Wi)(mod 11)
                //加权因子
                var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
                //校验位
                var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
                var sum = 0;
                var ai = 0;
                var wi = 0;
                for (var i = 0; i < 17; i++)
                {
                    ai = code[i];
                    wi = factor[i];
                    sum += ai * wi;
                }
                var last = parity[sum % 11];
                if(parity[sum % 11] != code[17]){
                    tip = "校验位错误";
                    pass =false;
                }
            }
        }
        return pass;
    }

    /**
     * 错误弹出层
     * @param  {string} msg  错误提示
     * @param  {int} time 几秒后退出，1000 等于1秒
     * @return {[type]}      [description]
     */
    huyue.popMsg = function(msg,time){
        var w = $(window).width();
        var h = $(window).height();
        if($(".msg20151118")){
            $(".msg20151118").remove();
        }
        var $box = $("<div class='msg20151118'></div>");
        $box.css({
            "background" : "rgba(0,0,0,.6)",
            "width" : "80%",
            "padding" : "10px",
            "line-height" : "24px",
            "font-size" : "14px",
            "text-align" : "center",
            "border-radius" : "10px",
            "color" : "#fff"
        })
        $box.text(msg)
        $("body").append($box);
        var width = $box.width();
        var height = $box.height();
        w = (w-width)/2;
        h = (h-height)/2;
        $box.css({
            "position" : "fixed",
            "top" : h,
            "left" : w
        })
        setTimeout(function(){
            $box.remove();
        },time || 3000);
    }

	window.huyue = window.hy = huyue;
})(window)